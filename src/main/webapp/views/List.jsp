<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <table>
        <th>ID</th>
        <th>List</th>
        <th>is_done</th>
            <c:forEach items="${list}" var="toDo">
                <tr>
                    <td>
                    ${toDo.id}
                    </td>

                   <td>${toDo.list}</td>
                
                
                <c:if test="${toDo.isDone == 0}">
                
                <td><a href="UpdateList?id=${toDo.id}&done=1">Done</a></td>
                </tr>
            	</c:if>
                <c:if test="${toDo.isDone == 1}">

                <td><a href="UpdateList?id=${toDo.id}&done=0">Is Not Done</a></td>
                </tr>
            	</c:if>
            </c:forEach>

    </table>

</body>
</html>